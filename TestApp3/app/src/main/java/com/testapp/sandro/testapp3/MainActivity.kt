package com.testapp.sandro.testapp3

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.google.gson.JsonObject
import com.koushikdutta.async.future.FutureCallback
import com.koushikdutta.ion.Ion
import java.security.AccessController.getContext
import com.google.gson.reflect.TypeToken
import com.google.gson.Gson
import org.json.JSONObject








class MainActivity : AppCompatActivity() {
    private lateinit var recyclerView: RecyclerView
    private lateinit var viewAdapter: RecyclerView.Adapter<*>
    private lateinit var viewManager: RecyclerView.LayoutManager


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


   //     var StringForAdapter = arrayOf("hello","booya","greetings","yoo")
      //  val ImagesForAdapter = arrayOf(R.mipmap.ic_launcher,R.mipmap.ic_launcher,R.mipmap.ic_launcher,R.mipmap.ic_launcher)
        val postList:ArrayList<RedditPostContainer> = ArrayList()
        //getting json
        val topPost = Ion.with(this)
            .load("https://www.reddit.com/r/dankmemes/top.json?limit=5")
            .asJsonObject().get()
        for(i in 0..4)
        {
            val topic =
                topPost.getAsJsonObject("data").getAsJsonArray("children")[i].asJsonObject.getAsJsonObject("data")
            //   Log.i("TestRedditApp",topic.asString)

            val imageUrl = topic["url"].asString
            val title = topic["title"].asString

            postList.add(
                RedditPostContainer(
                    RedditPostContainer.PostText(title),
                    RedditPostContainer.PostThumbnail(imageUrl)
                )
            )
        }



        viewManager = LinearLayoutManager(this)
        viewAdapter = MyAdapter(postList)

        recyclerView = findViewById<RecyclerView>(R.id.my_recycler_view).apply {
            // use this setting to improve performance if you know that changes
            // in content do not change the layout size of the RecyclerView
            setHasFixedSize(true)

            // use a linear layout manager
            layoutManager = viewManager

            // specify an viewAdapter (see also next example)
            adapter = viewAdapter


        }
    }
}
