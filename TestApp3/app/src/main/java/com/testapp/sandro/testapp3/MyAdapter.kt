package com.testapp.sandro.testapp3

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import android.widget.TextView
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.reyclce_view_layout.view.*


class MyAdapter(private val myDataset: ArrayList<RedditPostContainer>): RecyclerView.Adapter<MyAdapter.MyViewHolder>() {

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder.
    // Each data item is just a string in this case that is shown in a TextView.
    class MyViewHolder(val layoutForRecyclerView: View) : RecyclerView.ViewHolder(layoutForRecyclerView)


    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int): MyAdapter.MyViewHolder {
        // create a new view
        val layoutForRecyclerView = LayoutInflater.from(parent.context)
            .inflate(R.layout.reyclce_view_layout, parent, false) as View
        // set the view's size, margins, paddings and layout parameters
        return MyViewHolder(layoutForRecyclerView)
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        Picasso.get().
            load(myDataset[position].postThumnailObject.thumbNail).fit().centerCrop().into(holder.layoutForRecyclerView.ImageViewForRecycleView)
        holder.layoutForRecyclerView.textViewForRecycleView.text = myDataset[position].postTextObj.postTextString
    }

    // Return the size of your dataset (invoked by the layout manager)
    override fun getItemCount() = myDataset.size
}