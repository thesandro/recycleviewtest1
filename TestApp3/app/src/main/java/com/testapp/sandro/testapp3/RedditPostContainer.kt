package com.testapp.sandro.testapp3

import android.widget.ImageView

class RedditPostContainer(val postTextObj:PostText,val postThumnailObject:PostThumbnail)
{
    class PostText(val postTextString: String)
    class PostThumbnail(val thumbNail:String)
}